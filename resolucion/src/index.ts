type Destreza = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10;
enum Estados { vivo, muerto }

interface Personaje {
    nombre: string;
    familia: string;
    edad: number;
    estado: Estados;
    comunicar(): string;
}

class Rey implements Personaje {
    constructor(
        public nombre: string,
        public familia: string,
        public edad: number,
        public estado: Estados,
        public anyosReinado: number
    ) { }

    comunicar(): string {
        return "Vais a morir todos";
    }
}

class Luchador implements Personaje {
    public arma: string;
    public destreza: Destreza;

    constructor(
        public nombre: string,
        public familia: string,
        public edad: number,
        public estado: Estados
    ) { }

    comunicar(): string {
        return "Primero pego y luego pregunto";
    }
}

class Asesor implements Personaje {
    constructor(
        public nombre: string,
        public familia: string,
        public edad: number,
        public estado: Estados,
        public asesorado: Personaje
    ) { }

    comunicar(): string {
        return "No sé por qué, pero creo que voy a morir pronto";
    }
}

class Escudero implements Personaje {
    public pelotismo: number;

    constructor(
        public nombre: string,
        public familia: string,
        public edad: number,
        public estado: Estados,
        public amo: Luchador
    ) { }

    comunicar(): string {
        return "Soy un loser";
    }
}

let jamie = new Luchador("Jamie", "Lannister", 48, Estados.vivo);
let daenerys = new Luchador("Daenerys", "Targaryen", 35, Estados.vivo);
let tyrion = new Asesor("Tyrion", "Lannister", 46, Estados.vivo, daenerys);
let bronn = new Escudero("Bronn", "", 45, Estados.vivo, jamie);

let personajes: Array<Personaje> = [jamie, daenerys, tyrion, bronn];

function getMensajes(personajes: Personaje[]): string[] {

    let mensajes: string[] = personajes
        .filter(personaje => personaje instanceof Luchador)
        .map(personaje => personaje.comunicar());

    return mensajes;
}

let mensajes = getMensajes(personajes);
for (let mensaje of mensajes) {
    console.log(mensaje);
}