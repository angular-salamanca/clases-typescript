var Estados;
(function (Estados) {
    Estados[Estados["vivo"] = 0] = "vivo";
    Estados[Estados["muerto"] = 1] = "muerto";
})(Estados || (Estados = {}));
var Rey = /** @class */ (function () {
    function Rey(nombre, familia, edad, estado, anyosReinado) {
        this.nombre = nombre;
        this.familia = familia;
        this.edad = edad;
        this.estado = estado;
        this.anyosReinado = anyosReinado;
    }
    Rey.prototype.comunicar = function () {
        return "Vais a morir todos";
    };
    return Rey;
}());
var Luchador = /** @class */ (function () {
    function Luchador(nombre, familia, edad, estado) {
        this.nombre = nombre;
        this.familia = familia;
        this.edad = edad;
        this.estado = estado;
    }
    Luchador.prototype.comunicar = function () {
        return "Primero pego y luego pregunto";
    };
    return Luchador;
}());
var Asesor = /** @class */ (function () {
    function Asesor(nombre, familia, edad, estado, asesorado) {
        this.nombre = nombre;
        this.familia = familia;
        this.edad = edad;
        this.estado = estado;
        this.asesorado = asesorado;
    }
    Asesor.prototype.comunicar = function () {
        return "No sé por qué, pero creo que voy a morir pronto";
    };
    return Asesor;
}());
var Escudero = /** @class */ (function () {
    function Escudero(nombre, familia, edad, estado, amo) {
        this.nombre = nombre;
        this.familia = familia;
        this.edad = edad;
        this.estado = estado;
        this.amo = amo;
    }
    Escudero.prototype.comunicar = function () {
        return "Soy un loser";
    };
    return Escudero;
}());
var jamie = new Luchador("Jamie", "Lannister", 48, Estados.vivo);
var daenerys = new Luchador("Daenerys", "Targaryen", 35, Estados.vivo);
var tyrion = new Asesor("Tyrion", "Lannister", 46, Estados.vivo, daenerys);
var bronn = new Escudero("Bronn", "", 45, Estados.vivo, jamie);
var personajes = [jamie, daenerys, tyrion, bronn];
function getMensajes(personajes) {
    var mensajes = personajes
        .filter(function (personaje) { return personaje instanceof Luchador; })
        .map(function (personaje) { return personaje.comunicar(); });
    return mensajes;
}
var mensajes = getMensajes(personajes);
for (var _i = 0, mensajes_1 = mensajes; _i < mensajes_1.length; _i++) {
    var mensaje = mensajes_1[_i];
    console.log(mensaje);
}
//# sourceMappingURL=index.js.map